
/*
 *  Author: Michał Jarosz
 *  Model Name: device2ledger.spthy
 *  Status: DEVELOPMENT_AL
 *
 *  CommenT_:
 */

theory Device2Device
begin
functions:
aead/3, decrypt/3, verify/3, true/0, h/1
equations: decrypt(aead(k, p, a), a, k)=p
equations: verify(aead(k, p, a), a, k)=true

rule Setup:
    [ Fr(~keyA),Fr(~keyB) ]
--[ID_IoT_A($IoT_A,~keyA),ID_HLF($HLF,~keyA),ID_HLF($HLF,~keyB),ID_IoT_B($IoT_B,~keyB)]->
    [!Identity($IoT_A, ~keyA), !Identity($HLF, ~keyA),!Identity($HLF, ~keyB),!Identity($IoT_B, ~keyB)]

rule IoT_A_1:
    [ Fr(~nonce), !Identity($IoT_A, ~keyA), !Identity(HLF, ~keyA) ]
--[IoT_A_HLF($IoT_A,HLF,<~keyA,~nonce>)]->
    [ Out(<$IoT_A,HLF,~nonce,aead(~keyA,$IoT_B,~nonce)>) ]

rule HLF_1:
let EncMessage = aead(~keyA,IoT_B,~nonce)
keyAB = h(<IoT_A,IoT_B,~keyB>)
IoT_B = decrypt(EncMessage,~nonce,~keyA)
in
    [ Fr(~nonce2),!Identity($HLF, ~keyA),!Identity($HLF, ~keyB), In(<IoT_A,$HLF,~nonce,EncMessage>) ]
--[HLFIoT_A($HLF,IoT_A,<~keyA,~nonce2>),Ex(verify(EncMessage,~nonce,~keyA),true)]->
    [ Out(<$HLF,IoT_A,~nonce2,aead(~keyA,h(<IoT_A,IoT_B,~keyB>),~nonce2)>),!SessionKey(IoT_A,IoT_B,keyAB)]

rule IoT_A_2:
let EncMessageFromHLF= aead(~keyA,keyAB,~nonce2)
keyAB = decrypt(EncMessageFromHLF,~nonce2,~keyA)
in
    [ Fr(~nonce3), !Identity($IoT_A, ~keyA), In(<HLF,$IoT_A,~nonce2,EncMessageFromHLF>),!SessionKey($IoT_A,$IoT_B,keyAB) ]
--[Receive_HLF(HLF,$IoT_A,<~keyA,~nonce2>),IoT_A_IoT_B($IoT_A,$IoT_B,<keyAB,~nonce3>),Ex(verify(EncMessageFromHLF,~nonce2,~keyA),true),ID_IoT_AB($IoT_A,keyAB)]->
    [ Out(<$IoT_A, $IoT_B, ~nonce3, aead(keyAB,'Data',~nonce3)>),  ]

rule IoT_B_1:
let
//keyAB = h(<IoT_A,$IoT_B,~keyB>)
EncMessageFromIoT_A = aead(keyAB,'Data',~nonce3)
in
        [ Fr(~nonce4), !Identity($IoT_B, ~keyB), In(<$IoT_A,$IoT_B,~nonce3,EncMessageFromIoT_A>),!SessionKey($IoT_A,$IoT_B,keyAB)  ]
    --[IoT_B_IoT_A($IoT_B,$IoT_A,<keyAB,~nonce4>),Ex(verify(EncMessageFromIoT_A,~nonce3,keyAB),true),ID_IoT_AB($IoT_B,keyAB)]->
        [ Out(<$IoT_B, $IoT_A, ~nonce4, aead(keyAB,'Response',~nonce4)>)  ]

rule IoT_A_3:
let
//keyAB = h(<$IoT_A,$IoT_B,keyB>)
EncMessageFromIoT_B = aead(keyAB,'Response',~nonce4)
in
        [ !Identity($IoT_A, ~keyA), In(<$IoT_B,$IoT_A,~nonce4,EncMessageFromIoT_B>),!SessionKey($IoT_A,$IoT_B,keyAB)  ]
    --[Receive($IoT_B,$IoT_A,<keyAB,~nonce4>),Ex(verify(EncMessageFromIoT_B,~nonce4,keyAB),true)]->
        [  ]

restriction dual_IoT_A:
"
    All IoT key #i. (
        ID_IoT_A(IoT,key) @ #i
    ) ==> not (Ex #j IoT2 . ID_IoT_A(IoT2,key) @j )//& #i=#j)
"
            
restriction dual_IoT_B:
"
    All IoT key #i. (
        ID_IoT_B(IoT,key) @ #i
    ) ==> not (Ex #j IoT2 . ID_IoT_B(IoT2,key) @j )//& #i=#j)
"


lemma Key_Secrecy_HLF_A:
"
All IoT HLF Key nonce nonce2 #i #j .
(
    IoT_A_HLF(IoT,HLF,<Key,nonce>) @ #i &
    HLFIoT_A(HLF,IoT,<Key,nonce2>) @ #j &
    #i < #j &
    not (IoT = HLF)
)==> not(Ex #k1 . K(Key) @ #k1)
"

lemma Key_Secrecy_AB:
"
All IoT_A IoT_B Key nonce nonce2 #i #j .
(
    IoT_A_IoT_B(IoT_A,IoT_B,<Key,nonce>) @ #i &
    IoT_B_IoT_A(IoT_B,IoT_A,<Key,nonce2>) @ #j &
    #j < #i &
    not (IoT_A = IoT_B)
)==> not(Ex #k . K(Key) @ #k)
"

lemma noninjectiveagreement_IoT:
"
All IoT HLF key nonce #i . (
    IoT_A_HLF(IoT,HLF,<key,nonce>) @i
) ==> Ex #j nonce2 . HLFIoT_A(HLF,IoT,<key,nonce2>) @j & #i < #j & not(Ex #k1 . K(key) @ #k1)
"

lemma noninjectiveagreement_HLF:
"
All IoT HLF key nonce #i . (
    HLFIoT_A(HLF,IoT,<key,nonce>) @i
) ==> Ex #j . Receive_HLF(HLF,IoT,<key,nonce>) @j & #i < #j & not(Ex #k1 . K(key) @ #k1)
"

lemma noninjectiveagreement_A_B:
"
All IoT_A IoT_B key nonce #i . (
    IoT_A_IoT_B(IoT_A,IoT_B,<key,nonce>) @i
) ==> Ex #j nonce2 . IoT_B_IoT_A(IoT_B,IoT_A,<key,nonce2>) @j & #i < #j & not(Ex #k1 . K(key) @ #k1)
"

lemma noninjectiveagreement_B_A:
"
All IoT_A IoT_B key nonce #i . (
    IoT_B_IoT_A(IoT_B,IoT_A,<key,nonce>) @i
) ==> Ex #j. Receive(IoT_B,IoT_A,<key,nonce>) @j & #i < #j & not(Ex #k1 . K(key) @ #k1)
"

lemma injectiveagreement_IoT:
"
All IoT HLF key nonce #i . (
    IoT_A_HLF(IoT,HLF,<key,nonce>) @i
) ==> Ex #j nonce2 . HLFIoT_A(HLF,IoT,<key,nonce2>) @j & #i < #j & not (Ex IoT2 #i2. IoT_A_HLF(IoT2,HLF,<key,nonce>)@i2 & not (#i2 = #i)) & not(Ex #k1 . K(key) @ #k1)
"

lemma injectiveagreement_HLF:
"
All IoT HLF key nonce #i . (
    HLFIoT_A(HLF,IoT,<key,nonce>) @i
) ==> Ex #j . Receive_HLF(HLF,IoT,<key,nonce>) @j & #i < #j & not (Ex HLF2 #i2. HLFIoT_A(HLF2,IoT,<key,nonce>)@i2 & not (#i2 = #i)) & not(Ex #k1 . K(key) @ #k1)
"

lemma injectiveagreement_A_B:
"
All IoT_A IoT_B key nonce #i . (
    IoT_A_IoT_B(IoT_A,IoT_B,<key,nonce>) @i
) ==> Ex #j nonce2 . IoT_B_IoT_A(IoT_B,IoT_A,<key,nonce2>) @j & #i < #j & not (Ex IoT_A2 #i2. IoT_A_IoT_B(IoT_A2,IoT_B,<key,nonce>)@i2 & not (#i2 = #i))& not(Ex #k1 . K(key) @ #k1)
"

lemma injectiveagreement_B_A:
"
All IoT_A IoT_B key nonce #i . (
    IoT_B_IoT_A(IoT_B,IoT_A,<key,nonce>) @i
) ==> Ex #j. Receive(IoT_B,IoT_A,<key,nonce>) @j & #i < #j & not (Ex IoT_B2 #i2. IoT_B_IoT_A(IoT_B2,IoT_A,<key,nonce>)@i2 & not (#i2 = #i)) & not(Ex #k1 . K(key) @ #k1)
"


end